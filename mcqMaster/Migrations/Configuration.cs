namespace mcqMaster.Migrations
{
    using mcqMaster.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<mcqMaster.DAL.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        /// <summary>
        /// Populate the seed data for the database after migration
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(mcqMaster.DAL.ApplicationDbContext context)
        {
            List<IdentityRole> roles = new List<IdentityRole>
            {
                new IdentityRole { Name = "Admin" },
                new IdentityRole { Name = "Moderator" },
                new IdentityRole { Name = "User" }
            };
            roles.ForEach(role => context.Roles.AddOrUpdate(p => p.Name, role));
            context.SaveChanges();

            List<Subject> subjects = new List<Subject> {
                new Subject { Description = "Math"},
                new Subject { Description = "Biology"},
                new Subject { Description = "Chemistry"},
                new Subject { Description = "Physics"},
                new Subject { Description = "Psychology"},
                new Subject { Description = "History"},
                new Subject { Description = "Geography"},
                new Subject { Description = "Pop Culture"}
            };

            subjects.ForEach(subject => context.Subjects.AddOrUpdate(p => p.Description, subject));
            context.SaveChanges();

            List<Gender> genders = new List<Gender> {
                new Gender { Description = "Male"},
                new Gender { Description = "Female"},
                new Gender { Description = "Other"}
            };

            genders.ForEach(gender => context.Genders.AddOrUpdate(p => p.Description, gender));
            context.SaveChanges();
        }
    }
}
