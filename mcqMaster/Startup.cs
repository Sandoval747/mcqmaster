﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mcqMaster.Startup))]
namespace mcqMaster
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
