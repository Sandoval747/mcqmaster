﻿using mcqMaster.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mcqMaster.ViewModels
{
    /// <summary>
    /// The value fields for the Test Index Pages (Public and My Tests)
    /// </summary>
    public class TestIndexViewModel
    {
        public Test Test { get; set; }
        public List<string> Subjects { get; set; }
    }

    /// <summary>
    /// Form fields for the Select Questions part of the Create Test workflow
    /// </summary>
    public class SelectQuestionViewModel
    {
        public Question Question { get; set; }
        public bool IsSelected { get; set; }
    }

    /// <summary>
    /// Form fields for the Finalize Test part of the Create Test workflow
    /// </summary>
    public class FinalizeTestViewModel
    {
        public IPagedList<Question> Questions { get; set; }

        [Required]
        [Display(Name = "Test Name")]
        [StringLength(128, ErrorMessage = "Cannot be greater than 128 characters")]
        public String TestName { get; set; }

        [Display(Name = "Present All Questions?")]
        public bool PresentAll { get; set; }

        [Display(Name = "Make Public?")]
        public bool IsPublic { get; set; }
    }

    /// <summary>
    /// View values for the Test Details page
    /// </summary>
    public class TestDetailsViewModel
    {
        public IPagedList<Question> Questions { get; set; }
        public Test Test { get; set; }
    }

    /// <summary>
    /// View values for the Test Finished page
    /// </summary>
    public class TestFinishedViewModel
    {
        public Test Test { get; set; }
        public Attempt Attempt { get; set; }
        public int AnsweredQuestions { get; set; }
        public int CorrectResponses { get; set; }

        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double Score { get; set; }
    }

    /// <summary>
    /// ViewModel for the test rows on the User Statistics page
    /// </summary>
    public class UserTestStatisticsViewModel
    {
        public Test Test { get; set; }
        public List<string> Subjects { get; set; }
        public int CorrectResponses { get; set; }

        [Display(Name = "Date Taken")]
        public DateTime DateTaken { get; set; }

        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double Score { get; set; }
    }

    /// <summary>
    /// View values for the Test Statistics
    /// </summary>
    public class TestStatisticsViewModel
    {
        public Test Test { get; set; }

        public string Mode { get; set; }
        public IEnumerable<SelectListItem> Modes { get; set; }

        [Display(Name = "Average Score:")]
        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double AverageScore { get; set; }

        [Display(Name = "Times Taken:")]
        public int TimesTaken { get; set; }

        [Display(Name = "Unique Users:")]
        public int UniqueUsers { get; set; }

        public IPagedList<TestQuestionStatisticsViewModel> Questions { get; set; }
        public IPagedList<TestAttemptStatisticsViewModel> Attempts { get; set; }

        public TestStatisticsViewModel()
        {
            var options = new List<SelectListItem>();
            options.Add(new SelectListItem
            {
                Value = "overall",
                Text = "Overall"
            });
            options.Add(new SelectListItem
            {
                Value = "question",
                Text = "By Question"
            });
            options.Add(new SelectListItem
            {
                Value = "user",
                Text = "By User"
            });
            Modes = options;
        }
    }

    /// <summary>
    /// ViewModel for the rows in the table on Test Statistics - By Question
    /// </summary>
    public class TestQuestionStatisticsViewModel
    {
        public Question Question { get; set; }

        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double Score { get; set; }
    }

    /// <summary>
    /// ViewModel for the rows in Test Statistics - By User
    /// </summary>
    public class TestAttemptStatisticsViewModel
    {
        public Attempt Attempt { get; set; }

        public int CorrectResponses { get; set; }

        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double Score { get; set; }
    }
}