﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mcqMaster.ViewModels
{
    /// <summary>
    /// The ViewModel for the Sort Button partial view used to sort the tables in the app
    /// </summary>
    public class SortButton
    {
        public string Action { get; set; }
        public string Controller { get; set; }

        public string SortBy { get; set; }
        public bool IsAlphabetic { get; set; }
        public bool IsDefault { get; set; }
        public bool IsDefaultDescending { get; set; }
    }
}