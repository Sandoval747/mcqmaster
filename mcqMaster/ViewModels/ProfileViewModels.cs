﻿using mcqMaster.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mcqMaster.ViewModels
{
    /// <summary>
    /// Form fields for Edit Profile
    /// </summary>
    public class EditProfileViewModel
    {
        [Required]
        [StringLength(128)]
        public String AspNetUserId { get; set; }

        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }

        [Display(Name = "Favourite Subjects")]
        public string FavouriteSubjects { get; set; }

        public string About { get; set; }

        [Display(Name = "Make Date of Birth Public?")]
        public bool IsDobPublic { get; set; }

        [Display(Name = "Profile Picture URL")]
        public string PhotoPath { get; set; }

        public virtual Gender Gender { get; set; }
    }

    /// <summary>
    /// Form Fields for Account Settings
    /// </summary>
    public class AccountSettingsViewModel
    {
        [Required]
        [StringLength(128)]
        public String AspNetUserId { get; set; }

        public string UserName { get; set; }

        [Display(Name = "Profile Picture URL")]
        public string PhotoPath { get; set; }

        [Display(Name = "Make Profile Public?")]
        public bool IsProfilePublic { get; set; }

        [Display(Name = "Default New Questions to Public?")]
        public bool IsNewQuestionPublic { get; set; }
    }

    /// <summary>
    /// Form fields and variables for User Statistics view
    /// </summary>
    public class UserStatisticsViewModel
    {
        [StringLength(128)]
        public string AspNetUserId { get; set; }

        public string UserName { get; set; }
        public string PhotoPath { get; set; }

        public string Mode { get; set; }
        public IEnumerable<SelectListItem> Modes { get; set; }
        
        [Display(Name = "Average Score:")]
        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double AverageScore { get; set; }

        [Display(Name = "Tests Taken:")]
        public int TestsTaken { get; set; }

        [Display(Name = "Unique Tests:")]
        public int UniqueTests { get; set; }

        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public IPagedList<UserTestStatisticsViewModel> Tests { get; set; }

        /// <summary>
        /// Constructor sets up available dropdown options
        /// </summary>
        public UserStatisticsViewModel()
        {
            var options = new List<SelectListItem>();
            options.Add(new SelectListItem
            {
                Value = "overall",
                Text = "Overall"
            });
            options.Add(new SelectListItem
            {
                Value = "subject",
                Text = "By Subject"
            });
            options.Add(new SelectListItem
            {
                Value = "test",
                Text = "By Test"
            });
            Modes = options;
        }
    }
}