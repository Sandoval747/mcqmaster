﻿using mcqMaster.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.ViewModels
{
    /// <summary>
    /// Form fields for the Create Question View
    /// </summary>
    public class CreateQuestionViewModel
    {
        //QUESTION FIELDS
        public int Id { get; set; }

        [Display(Name = "Subject")]
        public int SubjectId { get; set; }

        [Display(Name = "Make Public?")]
        public bool IsPublic { get; set; }

        [Required]
        [Display(Name = "Question")]
        public String StemText { get; set; }

        [Display(Name = "Image URL (optional)")]
        public String GraphicPath { get; set; }

        public virtual Subject Subject { get; set; }

        //OPTION FIELDS
        [Required(ErrorMessage = "A correct answer must be selected")]
        public int CorrectAnswer { get; set; }
        [Required(ErrorMessage = "Option A is required")]
        [Display(Name = "A")]
        public String OptionText { get; set; }
        [Required(ErrorMessage = "Option B is required")]
        [Display(Name = "B")]
        public String OptionText1 { get; set; }
        [Required(ErrorMessage = "Option C is required")]
        [Display(Name = "C")]
        public String OptionText2 { get; set; }
        [Required(ErrorMessage = "Option D is required")]
        [Display(Name = "D")]
        public String OptionText3 { get; set; }
    }

    /// <summary>
    /// Form fields for rating questions on Question Details view
    /// </summary>
    public class QuestionDetailsViewModel
    {
        public virtual Question Question { get; set; }

        public List<Option> ShuffledOptions { get; set; }

        [Range(1,5, ErrorMessage = "Rating value is required")]
        public int Rating { get; set; }
    }

    /// <summary>
    /// Form Fields and values for Question Statistics view
    /// </summary>
    public class QuestionStatisticsViewModel
    {
        public Question Question { get; set; }

        [Display(Name = "Percent Correct:")]
        [DisplayFormat(DataFormatString = "{0:P1}", ApplyFormatInEditMode = true)]
        public double Score { get; set; }

        [Display(Name = "Rating:")]
        [DisplayFormat(DataFormatString = "{0:F1}", ApplyFormatInEditMode = true)]
        public double Rating { get; set; }

        [Display(Name = "Times Answered:")]
        public int ResponseCount { get; set; }

        [Display(Name = "Number of Tests:")]
        public int TestsCount { get; set; }

        public Dictionary<Option, double> OptionFrequencies { get; set; }
    }
}