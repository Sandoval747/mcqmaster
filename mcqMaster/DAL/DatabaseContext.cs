﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using mcqMaster.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace mcqMaster.DAL
{
    /// <summary>
    /// Defines the database tables that are available to the application
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Attempt> Attempts { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Response> Responses { get; set; }
        public DbSet<SharedTest> SharedTests { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }

        public ApplicationDbContext()
            : base("mcqMasterDb", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}