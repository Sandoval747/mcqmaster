﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Rating
    {
        public int Id { get; set; }

        [StringLength(128)]
        public String AspNetUserId { get; set; }

        public int QuestionId { get; set; }
        public int RatingScore { get; set; }

        public virtual Question Question { get; set; }
        public virtual ApplicationUser AspNetUser { get; set; }

        public Rating()
        {
            AspNetUserId = HttpContext.Current.User.Identity.GetUserId();
        }
    }
}