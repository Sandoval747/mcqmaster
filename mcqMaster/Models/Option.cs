﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Option
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }

        [Required]
        public String OptionText { get; set; }

        public bool IsCorrect { get; set; }

        public virtual Question Question { get; set; }
        public virtual ICollection<Response> Responses { get; set; }
    }
}