﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Report
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }

        [StringLength(128)]
        public String AspNetUserId { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public virtual Question Question { get; set; }
        public virtual ApplicationUser AspNetUser { get; set; }
    }
}