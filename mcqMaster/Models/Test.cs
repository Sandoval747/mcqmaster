﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Test
    {
        public int Id { get; set; }

        [StringLength(128)]
        public String AspNetUserId { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(128)]
        [Index("TestNameIndex", IsUnique = true)]
        [Display(Name = "Test Name")]
        public String TestName { get; set; }

        public bool PresentAll { get; set; }
        public bool IsPublic { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime UpdatedDate { get; set; }

        public virtual ApplicationUser AspNetUser { get; set; }
        public virtual ICollection<TestQuestion> TestQuestions { get; set; }
        public virtual ICollection<SharedTest> SharedTests { get; set; }
        public virtual ICollection<Attempt> Attempts { get; set; }

        public Test()
        {
            AspNetUserId = HttpContext.Current.User.Identity.GetUserId();
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}