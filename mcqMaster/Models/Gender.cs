﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Gender
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(32)]
        [Display(Name = "Gender")]
        public String Description { get; set; }

        //public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
    }
}