﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class SharedTest
    {
        public int Id { get; set; }
        public int TestId { get; set; }

        [Required]
        [StringLength(128)]
        public String AspNetUserId { get; set; }

        public int AttemptCount { get; set; }
        public int AttemptLimit { get; set; }

        public virtual Test Test { get; set; }
        public virtual ApplicationUser AspNetUser { get; set; }
    }
}