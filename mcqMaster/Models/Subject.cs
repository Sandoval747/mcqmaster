﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Subject
    {
        public int Id { get; set; }
        
        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(32)]
        [Display(Name ="Subject")]
        public String Description { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}