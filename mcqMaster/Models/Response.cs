﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Response
    {
        public int Id { get; set; }
        public int AttemptId { get; set; }
        public int OptionId { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public virtual Attempt Attempt { get; set; }
        public virtual Option Option { get; set; }
    }
}