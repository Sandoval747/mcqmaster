﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Attempt
    {
        public int Id { get; set; }
        public int TestId { get; set; }

        [StringLength(128)]
        public String AspNetUserId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        public virtual Test Test { get; set; }
        public virtual ApplicationUser AspNetUser { get; set; }
        public virtual ICollection<Response> Responses { get; set; }
    }
}