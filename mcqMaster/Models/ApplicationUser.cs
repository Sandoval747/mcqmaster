﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace mcqMaster.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //Account Details
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:D}")]
        public DateTime DateOfBirth { get; set; }

        public int GenderId { get; set; }

        [Display(Name = "Ban User")]
        public bool IsBanned { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime UpdatedDate { get; set; }

        [Required]
        [Display(Name = "Security Question")]
        public string SecurityQuestion { get; set; }

        [Required]
        [Display(Name = "Security Answer")]
        public string SecurityAnswer { get; set; }

        //Role?

        //Profile Details
        [Display(Name = "Favourite Subjects")]
        public string FavouriteSubjects { get; set; }

        public string About { get; set; }

        [Display(Name = "Make Profile Public?")]
        public bool IsProfilePublic { get; set; }

        [Display(Name = "Make Date of Birth Public?")]
        public bool IsDobPublic { get; set; }

        [Display(Name = "Default New Questions to Public?")]
        public bool IsNewQuestionPublic { get; set; }

        [Display(Name = "Profile Picture URL")]
        public string PhotoPath { get; set; }

        public virtual Gender Gender { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Notification> SentNotifications { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Test> Tests { get; set; }
        public virtual ICollection<SharedTest> SharedTests { get; set; }
        public virtual ICollection<Attempt> Attempts { get; set; }

        public ApplicationUser() : base()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}