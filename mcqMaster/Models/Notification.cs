﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mcqMaster.Models
{
    public class Notification
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public String AspNetUserId { get; set; }

        [Required]
        [StringLength(128)]
        public String SenderId { get; set; }

        [Required]
        public String NotificationText { get; set; }

        public DateTime ReadTimeStamp { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        //public virtual AspNetUser AspNetUser { get; set; }
        //public virtual AspNetUser Sender { get; set; }
    }
}