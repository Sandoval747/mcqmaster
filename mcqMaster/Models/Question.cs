﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace mcqMaster.Models
{
    public class Question
    {
        [Display(Name = "#")]
        public int Id { get; set; }
        public int SubjectId { get; set; }

        [StringLength(128)]
        public String AspNetUserId { get; set; }

        [Display(Name = "Public Question")]
        public bool IsPublic { get; set; }

        [Required]
        [Display(Name = "Question")]
        public String StemText { get; set; }

        public String GraphicPath { get; set; }

        [Required]
        [Display(Name = "Created On")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Display(Name = "Updated On")]
        public DateTime UpdatedDate { get; set; }

        public virtual ApplicationUser AspNetUser { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
        public virtual ICollection<Option> Options { get; set; }
        public virtual ICollection<TestQuestion> TestQuestions { get; set; }

        public Question()
        {
            AspNetUserId = HttpContext.Current.User.Identity.GetUserId();
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }
}