﻿using mcqMaster.DAL;
using mcqMaster.Models;
using mcqMaster.ViewModels;
using Microsoft.AspNet.Identity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace mcqMaster.Controllers
{
    /// <summary>
    /// Controller for Question functionality
    /// </summary>
    [Authorize]
    public class QuestionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly int PageSize = 20;

        /// <summary>
        /// Displays the Public Question index table
        /// </summary>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">The current page number</param>
        /// <returns>View of the Public Question Index</returns>
        [AllowAnonymous]
        public ActionResult Index(string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var questions = db.Questions.Where(q => q.IsPublic).Include(q => q.Subject);

            IPagedList<Question> questionIndex = prepareQuestionIndex(questions, searchTerm, sortBy, sortMode, page);

            return View(questionIndex);
        }

        /// <summary>
        /// Display the Personal Question Index
        /// </summary>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">The current page number</param>
        /// <returns>View of the My Questions page</returns>
        public ActionResult MyQuestions(string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var currentUser = User.Identity.GetUserId();
            var questions = db.Questions.Where(q => q.AspNetUserId == currentUser).Include(q => q.Subject);

            IPagedList<Question> questionIndex = prepareQuestionIndex(questions, searchTerm, sortBy, sortMode, page);

            return View(questionIndex);
        }

        /// <summary>
        /// Displays the details of the selected question
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <param name="showCorrect">Whether to bold the correct answer in the question display</param>
        /// <returns>A View of the question's details</returns>
        [AllowAnonymous]
        public ActionResult Details(int? id, bool showCorrect = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            if (!question.IsPublic && question.AspNetUserId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            QuestionDetailsViewModel model = new QuestionDetailsViewModel();
            model.Question = question;
            
            model.ShuffledOptions = question.Options.OrderBy(x => Guid.NewGuid()).ToList();
            foreach (var option in model.ShuffledOptions)
            {
                if (option.IsCorrect)
                {
                    Session["CorrectResponse"] = 'A' + model.ShuffledOptions.IndexOf(option);
                    break;
                }
            }
            ViewBag.ShowCorrect = showCorrect;

            return View(model);
        }

        /// <summary>
        /// Handles the rating of a question submitted by POST from the details page
        /// </summary>
        /// <param name="model">ViewModel containing teh form values (rating)</param>
        /// <param name="id">Question ID</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details (QuestionDetailsViewModel model, int id)
        {
            model.Question = db.Questions.Find(id);
            String currentUser = User.Identity.GetUserId();
            Rating rating = db.Ratings.SingleOrDefault(r => r.AspNetUserId == currentUser && r.QuestionId == model.Question.Id);

            if (ModelState.IsValid)
            {
                if (rating == null)
                {
                    rating = new Rating
                    {
                        Question = model.Question,
                        QuestionId = model.Question.Id,
                        RatingScore = model.Rating
                    };
                    db.Ratings.Add(rating);
                }
                else
                {
                    rating.RatingScore = model.Rating;
                }
                db.SaveChanges();
            }
            
            return RedirectToAction("Details");
        }

        /// <summary>
        /// Displays the Create question form
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Description");
            return View();
        }

        /// <summary>
        /// Handles POST submission of the Create Question form
        /// </summary>
        /// <param name="model">ViewModel for create question containing the form values</param>
        /// <returns>Redirects to my questions on success</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateQuestionViewModel model)
        {
            if (ModelState.IsValid)
            {
                Question question = new Question
                {
                    SubjectId = model.SubjectId,
                    Subject = model.Subject,
                    IsPublic = model.IsPublic,
                    GraphicPath = model.GraphicPath,
                    StemText = model.StemText
                };
                db.Questions.Add(question);

                Option option = new Option
                {
                    QuestionId = question.Id,
                    Question = question,
                    OptionText = model.OptionText,
                    IsCorrect = model.CorrectAnswer == 0
                };
                db.Options.Add(option);
                Option option2 = new Option
                {
                    QuestionId = question.Id,
                    Question = question,
                    OptionText = model.OptionText1,
                    IsCorrect = model.CorrectAnswer == 1
                };
                db.Options.Add(option2);
                Option option3 = new Option
                {
                    QuestionId = question.Id,
                    Question = question,
                    OptionText = model.OptionText2,
                    IsCorrect = model.CorrectAnswer == 2
                };
                db.Options.Add(option3);
                Option option4 = new Option
                {
                    QuestionId = question.Id,
                    Question = question,
                    OptionText = model.OptionText3,
                    IsCorrect = model.CorrectAnswer == 3
                };
                db.Options.Add(option4);

                db.SaveChanges();
                return RedirectToAction("MyQuestions");
            }

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Description", model.SubjectId);
            return View(model);
        }

        /// <summary>
        /// Displays and populates a form to edit a particular question
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <returns>The View for the Edit Question form</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            if (question.AspNetUserId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }

            CreateQuestionViewModel model = new CreateQuestionViewModel
            {
                Id = question.Id,
                Subject = question.Subject,
                SubjectId = question.SubjectId,
                IsPublic = question.IsPublic,
                StemText = question.StemText,
                GraphicPath = question.GraphicPath
            };
            List<Option> options = question.Options.ToList();
            model.OptionText = options[0].OptionText;
            model.OptionText1 = options[1].OptionText;
            model.OptionText2 = options[2].OptionText;
            model.OptionText3 = options[3].OptionText;
            for (int i = 0; i < options.Count; i++)
            {
                if (options[i].IsCorrect)
                {
                    model.CorrectAnswer = i;
                    break;
                }
            }

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Description", question.SubjectId);
            return View(model);
        }

        /// <summary>
        /// Handles POST submission of the Edit Question form
        /// </summary>
        /// <param name="model">The ViewModel containing the form values</param>
        /// <returns>Redirects to My Questions on success</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateQuestionViewModel model)
        {
            Question question = db.Questions.Find(model.Id);
            if (question.AspNetUserId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                question.Subject = model.Subject;
                question.SubjectId = model.SubjectId;
                question.StemText = model.StemText;
                question.GraphicPath = model.GraphicPath;
                question.IsPublic = model.IsPublic;

                List<Option> options = question.Options.ToList();
                options[0].OptionText = model.OptionText;
                options[1].OptionText = model.OptionText1;
                options[2].OptionText = model.OptionText2;
                options[3].OptionText = model.OptionText3;
                for (int i = 0; i < options.Count; i++)
                {
                    options[i].IsCorrect = false;
                    if (model.CorrectAnswer == i)
                    {
                        options[i].IsCorrect = true;
                    }
                }
                question.Options = options;
                question.UpdatedDate = DateTime.Now;

                db.SaveChanges();
                return RedirectToAction("MyQuestions");
            }
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Description", question.SubjectId);
            return View(question);
        }

        /// <summary>
        /// Display the form to delete a question
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <returns>View for question deletion</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            if (question.AspNetUserId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            return View(question);
        }

        /// <summary>
        /// Handles POST submission of the Delete Question form
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <returns>Redirects to My Questions on deletion success</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //TODO: REDIRECT TO INDEX IF ATTEMPT TO DELETE NON-OWNED QUESTION
            Question question = db.Questions.Find(id);
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("MyQuestions");
        }

        /// <summary>
        /// Populates the Question Statistics view with data
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <returns>View of teh Question statistics for this question</returns>
        [AllowAnonymous]
        public ActionResult Statistics (int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            var responseCount = db.Responses.Where(response => response.Option.QuestionId == question.Id).ToList().Count;

            var model = new QuestionStatisticsViewModel();
            model.Question = question;
            model.TestsCount = question.TestQuestions.Count;
            model.ResponseCount = responseCount;
            try
            {
                model.Rating = db.Ratings.Where(rating => rating.QuestionId == question.Id).Select(rating => rating.RatingScore).Average();
            }
            catch (Exception ex)
            {
                model.Rating = 0;
            }

            model.OptionFrequencies = new Dictionary<Option, double>();
            foreach (var option in question.Options)
            {
                var optionCount = db.Responses.Where(response => response.OptionId == option.Id).ToList().Count;
                model.OptionFrequencies.Add(option, (double)optionCount / responseCount);
                
                if (option.IsCorrect)
                {
                    model.Score = (double)optionCount / responseCount;
                }
            }

            return View(model);
        }

        /// <summary>
        /// Prepares a paged list of questions for the Public and "My" Questions indices
        /// </summary>
        /// <param name="questions">An unprepared list of questions</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current page</param>
        /// <returns>A paged list sorted and filtered by the given parameters</returns>
        private IPagedList<Question> prepareQuestionIndex(IQueryable<Question> questions, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Search
            if (!String.IsNullOrEmpty(searchTerm))
            {
                questions = questions.Where(q => q.Id.ToString().Contains(searchTerm) ||
                                                 q.StemText.Contains(searchTerm) ||
                                                 q.Subject.Description.Contains(searchTerm) ||
                                                 q.AspNetUser.UserName.Contains(searchTerm));
            }

            //Sort
            if (!String.IsNullOrEmpty(sortBy))
            {
                if (sortBy.Equals("subject"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.Subject.Description) :
                                questions.OrderBy(q => q.Subject.Description);
                }
                else if (sortBy.Equals("creator"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.AspNetUser.UserName) :
                                questions.OrderBy(q => q.AspNetUser.UserName);
                }
                else if (sortBy.Equals("question"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.StemText) :
                                questions.OrderBy(q => q.StemText);
                }
                else if (sortBy.Equals("rating"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.Ratings.Sum(r => r.RatingScore) / q.Ratings.Count) :
                                questions.OrderBy(q => q.Ratings.Sum(r => r.RatingScore) / q.Ratings.Count);
                }
                else if (sortBy.Equals("difficulty"))
                {
                    //TODO: SORT BY DIFFICULTY
                }
                else if (sortBy.Equals("updated"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.UpdatedDate) :
                                questions.OrderBy(q => q.UpdatedDate);
                }
            }
            else if (!String.IsNullOrEmpty(sortMode) && sortMode.Equals("desc"))
            {
                questions = questions.OrderByDescending(q => q.Id);
            }
            else
            {
                questions = questions.OrderBy(q => q.Id);
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return questions.ToPagedList(page, PageSize);
        }

        /// <summary>
        /// Accepts a option response and provides feedback without recording to database
        /// </summary>
        /// <param name="optionId">The OptionID selected</param>
        /// <returns>A JSON response indicating whether the selected option is correct</returns>
        public ActionResult PracticeResponse(int optionId)
        {
            var option = db.Options.Find(optionId);
            string response;
            if (option.IsCorrect)
            {
                response = option.IsCorrect.ToString();
            }
            else
            {
                response = ((char)(int)Session["CorrectResponse"]).ToString(); ;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
