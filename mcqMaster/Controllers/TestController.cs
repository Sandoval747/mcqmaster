﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mcqMaster.DAL;
using mcqMaster.Models;
using mcqMaster.ViewModels;
using Microsoft.AspNet.Identity;
using PagedList;

namespace mcqMaster.Controllers
{
    /// <summary>
    /// Controller for Test functionality
    /// </summary>
    [Authorize]
    public class TestController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly int PageSize = 20;

        /// <summary>
        /// Displays an index of Public Tests
        /// </summary>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>The View for the Public Tests index</returns>
        [AllowAnonymous]
        public ActionResult Index(string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var tests = db.Tests.Where(t => t.IsPublic).ToList();

            var testIndex = prepareTestIndex(tests, searchTerm, sortBy, sortMode, page);

            return View(testIndex);
        }

        /// <summary>
        /// Displays the index for My Tests page
        /// </summary>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>The View for My Tests</returns>
        public ActionResult MyTests(string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var currentUser = User.Identity.GetUserId();
            var tests = db.Tests.Where(q => q.AspNetUserId == currentUser).ToList();

            var testIndex = prepareTestIndex(tests, searchTerm, sortBy, sortMode, page);

            return View(testIndex);
        }

        /// <summary>
        /// Displays the test details view for the given test
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>The Details view for the specified test</returns>
        [AllowAnonymous]
        public ActionResult Details(int? id, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }

            var selectedQuestions = test.TestQuestions.Select(tq => tq.QuestionId).ToList();
            var testDetailsViewModel = new TestDetailsViewModel
            {
                Test = test
            };
            testDetailsViewModel.Questions = prepareQuestionIndex(db.Questions.Where(q => selectedQuestions.Contains(q.Id)).OrderBy(q => q.Id), searchTerm, sortBy, sortMode, page).ToPagedList(page, PageSize);

            return View(testDetailsViewModel);
        }

        /// <summary>
        /// Sets up Test Creation and redirects the user to Select Questions
        /// </summary>
        /// <returns>Redirect to Select Questions</returns>
        public ActionResult Create()
        {
            Session["SelectedQuestions"] = new List<int>();
            return RedirectToAction("SelectQuestions");
        }

        /// <summary>
        /// Sets up Test Editing and redirects the user to Test Finalization
        /// </summary>
        /// <param name="id">Test ID to edit</param>
        /// <returns>Redirect to Test Finalize</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            Session["SelectedQuestions"] = test.TestQuestions.Select(tq => tq.QuestionId).ToList();
            return RedirectToAction("Finalize", new { id });
        }

        /// <summary>
        /// Returns the view that allows the user to select questions for the test (edit or create)
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort questions on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current page</param>
        /// <returns>The view for Question selection</returns>
        public ActionResult SelectQuestions(int? id, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            ViewBag.EditMode = id == null ? false : true;
            var currentUser = User.Identity.GetUserId();
            var questions = db.Questions.Where(q => q.AspNetUserId == currentUser || q.IsPublic).Include(q => q.Subject);

            var questionIndex = prepareQuestionIndex(questions, searchTerm, sortBy, sortMode, page);

            //Convert to SelectQuestionViewModel list
            if (Session["SelectedQuestions"] == null)
            {
                Session["SelectedQuestions"] = new List<int>();
            }

            var selectedQuestions = (List<int>)Session["SelectedQuestions"];
            var selectableQuestions = new List<SelectQuestionViewModel>();
            foreach (Question question in questionIndex)
            {
                var selectableQuestion = new SelectQuestionViewModel();
                selectableQuestion.Question = question;

                if (selectedQuestions.Contains(question.Id))
                {
                    selectableQuestion.IsSelected = true;
                }

                selectableQuestions.Add(selectableQuestion);
            }
            var selectableQuestionIndex = selectableQuestions.ToPagedList(page, PageSize);

            ViewBag.Page = page;
            ViewBag.SelectedQuestions = selectedQuestions.Count;

            return View(selectableQuestionIndex);
        }

        /// <summary>
        /// Returns the view for Test Finalization during create/edit test
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>A View for the Finalization form for test create/edit</returns>
        public ActionResult Finalize(int? id, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            ViewBag.EditMode = id == null ? false : true;
            var selectedQuestions = (List<int>)Session["SelectedQuestions"];
            if (selectedQuestions == null)
            {
                return RedirectToAction("MyTests");
            }
            if (selectedQuestions.Count <= 0)
            {
                return RedirectToAction("MyTests");
            }

            FinalizeTestViewModel model;
            if (id == null)
            {
                model = new FinalizeTestViewModel();
                model.PresentAll = true;
            }
            else
            {
                Test test = db.Tests.Find(id);
                if (test == null)
                {
                    return HttpNotFound();
                }
                if (test.AspNetUserId != User.Identity.GetUserId())
                {
                    return RedirectToAction("Index");
                }
                model = new FinalizeTestViewModel
                {
                    TestName = test.TestName,
                    IsPublic = test.IsPublic,
                    PresentAll = test.PresentAll
                };
            }
            
            var questions = db.Questions.Where(q => selectedQuestions.Contains(q.Id));

            model.Questions = prepareQuestionIndex(questions, searchTerm, sortBy, sortMode, page).ToPagedList(page, PageSize);

            ViewBag.Page = page;

            return View(model);
        }

        /// <summary>
        /// Handles POST submission of the Finalize Test form
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <param name="model">ViewModel containing form values</param>
        /// <returns>Redirects to My Test on success</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Finalize(int? id, FinalizeTestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var selectedQuestions = (List<int>)Session["SelectedQuestions"];
                if (id == null)
                {
                    var test = new Test
                    {
                        TestName = model.TestName,
                        PresentAll = model.PresentAll,
                        IsPublic = model.IsPublic
                    };
                    db.Tests.Add(test);

                    foreach (int questionId in selectedQuestions)
                    {
                        var testQuestion = new TestQuestion
                        {
                            TestId = test.Id,
                            QuestionId = questionId
                        };
                        db.TestQuestions.Add(testQuestion);
                    }
                }
                else
                {
                    var test = db.Tests.Find(id);

                    test.TestName = model.TestName;
                    test.IsPublic = model.IsPublic;
                    test.PresentAll = model.PresentAll;
                    test.TestQuestions = new List<TestQuestion>();

                    var existingTestQuestions = db.TestQuestions.Where(tq => tq.TestId == test.Id).ToList();
                    foreach (TestQuestion testQuestion in existingTestQuestions)
                    {
                        if (!selectedQuestions.Contains(testQuestion.QuestionId))
                        {
                            db.TestQuestions.Remove(testQuestion);
                        }
                    }

                    var existingTestQuestionIds = existingTestQuestions.Select(tq => tq.QuestionId).ToList();
                    foreach (int questionId in selectedQuestions)
                    {
                        if (!existingTestQuestionIds.Contains(questionId))
                        {
                            var testQuestion = new TestQuestion
                            {
                                TestId = test.Id,
                                QuestionId = questionId
                            };
                            db.TestQuestions.Add(testQuestion);
                        }                        
                    }
                }

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException ex)
                {
                    ModelState.AddModelError("TestName", model.TestName + " already exists");
                    model.Questions = db.Questions.Where(q => selectedQuestions.Contains(q.Id)).OrderBy(q => q.Id).ToPagedList(1, PageSize);
                    return View(model);
                }
                return RedirectToAction("MyTests");
            }

            return View(model);
        }

        // POST: Test/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AspNetUserId,TestName,PresentAll,IsPublic,CreatedDate,UpdatedDate")] Test test)
        {
            if (ModelState.IsValid)
            {
                db.Entry(test).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(test);
        }

        /// <summary>
        /// Shows the Delete test view
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <returns>View to Delete a test</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            if (test.AspNetUserId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            return View(test);
        }

        /// <summary>
        /// Handles POST submission of Delete test form
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <returns>Redirects to Public Tests on successful deletion</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Test test = db.Tests.Find(id);
            db.Tests.Remove(test);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Handles onclick POST for the toggling of a question selection on Select Questions view
        /// </summary>
        /// <param name="id">Question ID toggled</param>
        /// <param name="isSelected">Value indicating if question is currently selected or not</param>
        /// <returns>JSON response indicating success or failure to update the session variable</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToggleQuestionSelected(string id, bool isSelected)
        {
            try
            {
                int questionId;
                Int32.TryParse(id.Substring(23, id.Length - 23), out questionId);

                var selectedQuestions = (List<int>)Session["SelectedQuestions"];
                bool isAlreadySelected = selectedQuestions.Contains(questionId);

                if (isSelected && !isAlreadySelected)
                {
                    selectedQuestions.Add(questionId);
                }
                if (!isSelected && isAlreadySelected)
                {
                    selectedQuestions.Remove(questionId);
                }
                return Json(selectedQuestions.Count.ToString(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Failed to update selected questions: " + ex.Message);
            }
        }

        /// <summary>
        /// Show view of pre-test stats and confirmation form
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <returns>View of the pre-test screen</returns>
        public ActionResult TakeTest (int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var test = db.Tests.Find(id);

            return View(test);
        }

        /// <summary>
        /// Handles POST submission of confirmation that the test should be taken
        /// </summary>
        /// <param name="model">Test model containing form values</param>
        /// <returns>Sets up and redirects to the question display</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TakeTest([Bind(Include = "Id")]Test model)
        {
            var test = db.Tests.Find(model.Id);
            var questionQueue = new Queue<int>();
            var randomizedQuestions = test.TestQuestions.OrderBy(x => Guid.NewGuid()).ToList();
            foreach (var question in randomizedQuestions)
            {
                questionQueue.Enqueue(question.QuestionId);
            }

            var attempt = new Attempt
            {
                TestId = model.Id,
                AspNetUserId = User.Identity.GetUserId(),
                StartTime = DateTime.Now
            };

            Session["Attempt"] = attempt;
            Session["QuestionQueue"] = questionQueue;
            Session["Responses"] = new List<int>();

            return RedirectToAction("DisplayQuestion", new { id = model.Id });
        }

        /// <summary>
        /// Show the question during test taking
        /// </summary>
        /// <param name="id">Question ID</param>
        /// <returns>View to answer the test question</returns>
        [Route("Test/TakeTest/{id}/DisplayQuestion")]
        public ActionResult DisplayQuestion (int? id)
        {
            try
            {
                var questionQueue = (Queue<int>)Session["QuestionQueue"];
                var question = db.Questions.Find(questionQueue.Peek());

                var model = new QuestionDetailsViewModel();
                model.Question = question;

                model.ShuffledOptions = question.Options.OrderBy(x => Guid.NewGuid()).ToList();
                foreach (var option in model.ShuffledOptions)
                {
                    if (option.IsCorrect)
                    {
                        Session["CorrectResponse"] = 'A' + model.ShuffledOptions.IndexOf(option);
                        break;
                    }
                }

                return View(model);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Displays Test Finished View with performance statistics
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <returns>View of the Test Finished screen</returns>
        [Route("Test/TakeTest/{id}/TestFinished")]
        public ActionResult TestFinished(int? id)
        {
            try
            {
                var attemptId = ((Attempt)Session["Attempt"]).Id;
                var attempt = db.Attempts.Find(attemptId);

                int correctResponses = 0;
                foreach (Response response in attempt.Responses)
                {
                    var option = db.Options.Find(response.OptionId);
                    if (option.IsCorrect)
                    {
                        correctResponses++;
                    }
                }

                var model = new TestFinishedViewModel
                {
                    Test = db.Tests.Find(attempt.TestId),
                    Attempt = attempt,
                    AnsweredQuestions = attempt.Responses.Count,
                    CorrectResponses = correctResponses,
                    Score = (double)correctResponses / attempt.Responses.Count
                };

                return View(model);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Handles AJAX POST of the Next Question button during Test Taking
        /// </summary>
        /// <returns>JSON response for success</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NextQuestion()
        {
            var questionQueue = (Queue<int>)Session["QuestionQueue"];
            questionQueue.Dequeue();
            if (questionQueue.Count > 0)
            {                
                return Json("QuestionAdvanced", JsonRequestBehavior.AllowGet);
            }
            else
            {
                var attempt = (Attempt)Session["Attempt"];
                attempt.EndTime = DateTime.Now;

                db.Attempts.Add(attempt);

                var responseOptionIds = (List<int>)Session["Responses"];
                Response response;
                foreach (int optionId in responseOptionIds)
                {
                    response = new Models.Response
                    {
                        AttemptId = attempt.Id,
                        OptionId = optionId,
                        CreatedDate = DateTime.Now
                    };
                    db.Responses.Add(response);
                }
                db.SaveChanges();

                return Json(attempt.TestId, JsonRequestBehavior.AllowGet);
            }            
        }

        /// <summary>
        /// Handles AJAX POST of the submit respnse button during test taking
        /// </summary>
        /// <param name="optionId">Option ID that was selected</param>
        /// <returns>JSON response whether answer was correct or not</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitResponse(int optionId)
        {
            var questionResponses = (List<int>)Session["Responses"];
            var option = db.Options.Find(optionId);
            questionResponses.Add(optionId);

            string response;
            if (option.IsCorrect)
            {
                response = option.IsCorrect.ToString();
            }
            else
            {
                response = ((char)(int)Session["CorrectResponse"]).ToString(); ;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Displays and populates View showing Test statistics
        /// </summary>
        /// <param name="id">Test ID</param>
        /// <param name="mode">Display "Mode" i.e., overall/by question/by user</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current page</param>
        /// <returns>A View for the Test Statistics customized as per the parameters</returns>
        [AllowAnonymous]
        public ActionResult Statistics(int? id, string mode, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }

            var model = new TestStatisticsViewModel();
            model.Test = test;
            model.TimesTaken = test.Attempts.Count;
            model.UniqueUsers = test.Attempts.GroupBy(attempt => attempt.AspNetUserId).Select(g => g.First()).ToList().Count;

            var responseCount = test.Attempts.Select(attempt => attempt.Responses.Count).Sum();
            var correctReponses = test.Attempts.Select(attempt => attempt.Responses.Where(response => response.Option.IsCorrect).ToList().Count).Sum();
            model.AverageScore = (double)correctReponses / responseCount;


            if (mode != null)
            {
                if (mode.Equals("user"))
                {
                    var attempts = new List<TestAttemptStatisticsViewModel>();
                    foreach (var attempt in test.Attempts)
                    {
                        var attemptViewModel = new TestAttemptStatisticsViewModel
                        {
                            Attempt = attempt
                        };
                        attemptViewModel.CorrectResponses = attempt.Responses.Where(response => response.Option.IsCorrect).ToList().Count;
                        attemptViewModel.Score = (double)attemptViewModel.CorrectResponses / attempt.Responses.Count;
                        attempts.Add(attemptViewModel);
                    }
                    model.Attempts = prepareAttemptStatistics(attempts, searchTerm, sortBy, sortMode, page);
                }
                else if (mode.Equals("question"))
                {
                    var questions = new List<TestQuestionStatisticsViewModel>();
                    foreach (var testQuestion in test.TestQuestions)
                    {
                        var question = db.Questions.Find(testQuestion.QuestionId);
                        var questionViewModel = new TestQuestionStatisticsViewModel
                        {
                            Question = question
                        };

                        var correctResponses = 0;
                        var attempts = db.Attempts.Where(a => a.TestId == test.Id).ToList();
                        foreach (var attempt in attempts)
                        {
                            var responses = db.Responses.Where(r => r.AttemptId == attempt.Id).ToList();
                            foreach (var response in responses)
                            {
                                var option = db.Options.Find(response.OptionId);
                                if (option.IsCorrect)
                                {
                                    correctReponses++;
                                }
                            }
                        }
                        questionViewModel.Score = (double)correctResponses / test.Attempts.Count;
                        questions.Add(questionViewModel);
                    }
                    model.Questions = prepareQuestionStatistics(questions, searchTerm, sortBy, sortMode, page);
                }
            }           

            ViewBag.SelectedMode = mode;

            ViewBag.Mode = new SelectList(model.Modes, "Value", "Text", mode);

            return View(model);
        }

        /// <summary>
        /// Prepares a paged list of Question Statistics for display on Test Statistics - by Question
        /// </summary>
        /// <param name="questions">An unorganized list of questions</param>
        /// <param name="searchTerm">Search term to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or descending</param>
        /// <param name="page">Current page</param>
        /// <returns>A paged list of question stats view models</returns>
        private IPagedList<TestQuestionStatisticsViewModel> prepareQuestionStatistics(List<TestQuestionStatisticsViewModel> questions, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Search
            if (searchTerm != null)
            {
                questions = questions.Where(q => q.Question.StemText.Contains(searchTerm) ||
                                                 q.Question.Subject.Description.Contains(searchTerm) ||
                                                 q.Question.AspNetUser.UserName.Contains(searchTerm)).ToList();
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("score"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.Score).ToList() :
                                questions.OrderBy(q => q.Score).ToList();
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                questions = questions.OrderByDescending(q => q.Question.Id).ToList();
            }
            else
            {
                questions = questions.OrderBy(q => q.Question.Id).ToList();
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return questions.ToPagedList(page, PageSize);
        }

        /// <summary>
        /// Prepares an IPagedList of Attempt statistics for display on Test Statistics - by User
        /// </summary>
        /// <param name="attempts">An unorganized list of attempts</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>A paged list of viewmodels for display</returns>
        private IPagedList<TestAttemptStatisticsViewModel> prepareAttemptStatistics(List<TestAttemptStatisticsViewModel> attempts, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Search
            if (searchTerm != null)
            {
                attempts = attempts.Where(a => a.Attempt.AspNetUser.UserName.Contains(searchTerm)).ToList();
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("score"))
                {
                    attempts = sortMode != null && sortMode.Equals("desc") ?
                               attempts.OrderByDescending(a => a.Score).ToList() :
                               attempts.OrderBy(a => a.Score).ToList();
                }
                else if (sortBy.Equals("datetaken"))
                {
                    attempts = sortMode != null && sortMode.Equals("desc") ?
                               attempts.OrderByDescending(a => a.Attempt.EndTime).ToList() :
                               attempts.OrderBy(a => a.Attempt.EndTime).ToList();
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                attempts = attempts.OrderByDescending(a => a.Attempt.AspNetUser.UserName).ToList();
            }
            else
            {
                attempts = attempts.OrderBy(a => a.Attempt.AspNetUser.UserName).ToList();
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return attempts.ToPagedList(page, PageSize);
        }

        /// <summary>
        /// Prepares an sorted, filtered of Questions to display on the Select Questions page
        /// </summary>
        /// <param name="questions">An unorganized list of questions</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>A list of questions to display</returns>
        private IQueryable<Question> prepareQuestionIndex(IQueryable<Question> questions, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Search
            if (searchTerm != null)
            {
                questions = questions.Where(q => q.StemText.Contains(searchTerm) ||
                                                 q.Subject.Description.Contains(searchTerm) ||
                                                 q.AspNetUser.UserName.Contains(searchTerm));
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("subject"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.Subject.Description) :
                                questions.OrderBy(q => q.Subject.Description);
                }
                else if (sortBy.Equals("creator"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.AspNetUser.UserName) :
                                questions.OrderBy(q => q.AspNetUser.UserName);
                }
                else if (sortBy.Equals("question"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.StemText) :
                                questions.OrderBy(q => q.StemText);
                }
                else if (sortBy.Equals("rating"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.Ratings.Sum(r => r.RatingScore) / q.Ratings.Count) :
                                questions.OrderBy(q => q.Ratings.Sum(r => r.RatingScore) / q.Ratings.Count);
                }
                else if (sortBy.Equals("difficulty"))
                {
                    //TODO: SORT BY DIFFICULTY
                }
                else if (sortBy.Equals("updated"))
                {
                    questions = sortMode != null && sortMode.Equals("desc") ?
                                questions.OrderByDescending(q => q.UpdatedDate) :
                                questions.OrderBy(q => q.UpdatedDate);
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                questions = questions.OrderByDescending(q => q.Id);
            }
            else
            {
                questions = questions.OrderBy(q => q.Id);
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;

            return questions;
        }

        /// <summary>
        /// Prepares an IPagedList of Tests to display on the test index pages
        /// </summary>
        /// <param name="tests">An unorganized list of tests</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or Descending</param>
        /// <param name="page">Current Page</param>
        /// <returns>A paged list of tests to display</returns>
        private IPagedList<TestIndexViewModel> prepareTestIndex(List<Test> tests, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Convert to TestIndexViewModel
            var testIndexViewModels = new List<TestIndexViewModel>();
            for (int i = 0; i < tests.Count; i++)
            {
                var testIndexViewModel = new TestIndexViewModel();
                testIndexViewModel.Test = tests[i];
                testIndexViewModel.Subjects = new List<string>();

                var questionIds = tests[i].TestQuestions.Select(tq => tq.QuestionId).ToList();
                var questions = db.Questions.Where(q => questionIds.Contains(q.Id)).ToList();
                for (int j = 0; j < questions.Count; j++)
                {
                    if (!testIndexViewModel.Subjects.Contains(questions[j].Subject.Description))
                    {
                        testIndexViewModel.Subjects.Add(questions[j].Subject.Description);
                    }
                }

                //Reverse sort Subjects if page is sorted by subject in descending order, else sort in alpha order
                testIndexViewModel.Subjects = sortBy != null && sortBy.Equals("subject") && sortMode != null && sortMode.Equals("desc") ?
                                              testIndexViewModel.Subjects.OrderByDescending(s => s).ToList() :
                                              testIndexViewModel.Subjects.OrderBy(s => s).ToList();

                testIndexViewModels.Add(testIndexViewModel);
            }

            //Search
            if (searchTerm != null)
            {
                testIndexViewModels = testIndexViewModels.Where(t => t.Test.TestName.ToLower().Contains(searchTerm.ToLower()) ||
                                                                     t.Test.AspNetUser.UserName.ToLower().Contains(searchTerm.ToLower()) ||
                                                                     t.Test.TestQuestions.Count.ToString().ToLower().Contains(searchTerm.ToLower()) ||
                                                                     t.Subjects.Where(s => s.ToLower().Contains(searchTerm.ToLower())).ToList().Count > 0).ToList();
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("subject"))
                {
                    testIndexViewModels = sortMode != null && sortMode.Equals("desc") ?
                                          testIndexViewModels.OrderByDescending(t => t.Subjects.First()).ToList() :
                                          testIndexViewModels.OrderBy(t => t.Subjects.First()).ToList();
                }
                else if (sortBy.Equals("creator"))
                {
                    testIndexViewModels = sortMode != null && sortMode.Equals("desc") ?
                                          testIndexViewModels.OrderByDescending(t => t.Test.AspNetUser.UserName).ToList() :
                                          testIndexViewModels.OrderBy(t => t.Test.AspNetUser.UserName).ToList();
                }
                else if (sortBy.Equals("questions"))
                {
                    testIndexViewModels = sortMode != null && sortMode.Equals("desc") ?
                                          testIndexViewModels.OrderByDescending(t => t.Test.TestQuestions.Count).ToList() :
                                          testIndexViewModels.OrderBy(t => t.Test.TestQuestions.Count).ToList();
                }
                else if (sortBy.Equals("difficulty"))
                {
                    //TODO: SORT BY DIFFICULTY
                }
                else if (sortBy.Equals("updated"))
                {
                    testIndexViewModels = sortMode != null && sortMode.Equals("desc") ?
                                          testIndexViewModels.OrderByDescending(t => t.Test.UpdatedDate).ToList() :
                                          testIndexViewModels.OrderBy(t => t.Test.UpdatedDate).ToList();
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                testIndexViewModels = testIndexViewModels.OrderByDescending(t => t.Test.TestName).ToList();
            }
            else
            {
                testIndexViewModels = testIndexViewModels.OrderBy(t => t.Test.TestName).ToList();
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return testIndexViewModels.ToPagedList(page, PageSize);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
