﻿using mcqMaster.DAL;
using mcqMaster.Models;
using mcqMaster.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace mcqMaster.Controllers
{
    /// <summary>
    /// Controller for Profile functionality
    /// </summary>
    [Authorize]
    public class ProfileController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private static readonly int PageSize = 20;

        /// <summary>
        /// Shows the default profile page for a user
        /// </summary>
        /// <param name="username">Which user to show the profile for, default is the current user</param>
        /// <returns>The View for the main profile page</returns>
        [AllowAnonymous]
        public ActionResult Index(string username)
        {
            ApplicationUser user;
            if (username == null)
            {
                user = db.Users.Find(User.Identity.GetUserId());
            }
            else
            {
                user = db.Users.Where(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return HttpNotFound();
                }
                if (!user.IsProfilePublic)
                {
                    return HttpNotFound();
                }
            }

            ViewBag.Age = calculateAge(user.DateOfBirth);

            return View(user);
        }

        /// <summary>
        /// Shows the edit page for the user's profile
        /// </summary>
        /// <returns>The View for the Edit Profile Page</returns>
        public ActionResult Edit()
        {
            var user = db.Users.Find(User.Identity.GetUserId());

            var model = new EditProfileViewModel
            {
                AspNetUserId = user.Id,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                GenderId = user.GenderId,
                Gender = user.Gender,
                PhotoPath = user.PhotoPath,
                IsDobPublic = user.IsDobPublic,
                FavouriteSubjects = user.FavouriteSubjects,
                About = user.About
            };

            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Description", user.GenderId);

            return View(model);
        }

        /// <summary>
        /// Handles POST submission of the Edit Profile form
        /// </summary>
        /// <param name="model">The ViewModel of the Edit page containing the form data</param>
        /// <returns>Redirects the user to the main profile page on success</returns>
        [HttpPost]
        public ActionResult Edit(EditProfileViewModel model)
        {
            var user = db.Users.Find(model.AspNetUserId);
            if (ModelState.IsValid)
            {               

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhotoPath = model.PhotoPath;
                user.GenderId = model.GenderId;
                user.Gender = model.Gender;
                user.IsDobPublic = model.IsDobPublic;
                user.FavouriteSubjects = model.FavouriteSubjects;
                user.About = model.About;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Description", user.GenderId);
            return View(model);
        }

        /// <summary>
        /// Displays the Account Settings profile page
        /// </summary>
        /// <returns>The View for the Account Settings page</returns>
        public ActionResult AccountSettings()
        {
            var user = db.Users.Find(User.Identity.GetUserId());

            var model = new AccountSettingsViewModel
            {
                AspNetUserId = user.Id,
                UserName = user.UserName,
                PhotoPath = user.PhotoPath,
                IsProfilePublic = user.IsProfilePublic,
                IsNewQuestionPublic = user.IsNewQuestionPublic
            };

            return View(model);
        }

        /// <summary>
        /// Handles POST submission of the Account Settings page
        /// </summary>
        /// <param name="model">The ViewModel for the account settings containing the form values</param>
        /// <returns>Redirects to the main profile page on success</returns>
        [HttpPost]
        public ActionResult AccountSettings(AccountSettingsViewModel model)
        {
            var user = db.Users.Find(model.AspNetUserId);
            if (ModelState.IsValid)
            {

                user.IsProfilePublic = model.IsProfilePublic;
                user.IsNewQuestionPublic = model.IsNewQuestionPublic;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Populates the User Statistics page of the profile with the user's data, filtered using the parameters
        /// </summary>
        /// <param name="mode">The mode that the stats page is in i.e., overall/by subject/by test</param>
        /// <param name="subject">The currently selected subject to filter by</param>
        /// <param name="testId">The testId to filter by</param>
        /// <param name="searchTerm">The search string to filter by</param>
        /// <param name="sortBy">Which column to sort the results on</param>
        /// <param name="sortMode">Whether to sort ascending or descending</param>
        /// <param name="page">The current page number</param>
        /// <returns>A View of the user's statistics</returns>
        public ActionResult Statistics(string mode, int? subject, int? testId, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var user = db.Users.Find(User.Identity.GetUserId());
            List<Attempt> attempts = db.Attempts.Where(attempt => attempt.AspNetUserId.Equals(user.Id)).ToList();
            var responses = db.Responses.Where(response => response.Attempt.AspNetUserId.Equals(user.Id)).ToList();

            var model = new UserStatisticsViewModel
            {
                AspNetUserId = user.Id,
                UserName = user.UserName,
                PhotoPath = user.PhotoPath,
                Tests = new List<UserTestStatisticsViewModel>().ToPagedList(1, PageSize)
            };

            if (mode != null && mode.Equals("subject") && subject != null)
            {
                responses = responses.Where(response => response.Option.Question.Subject.Id == subject).ToList();

                var attemptIds = new List<int>();
                foreach (Response response in responses)
                {
                    attemptIds.Add(response.Attempt.Id);
                }

                attempts = attempts.Where(attempt => attemptIds.Contains(attempt.Id)).ToList();
            }
            else if(mode != null && mode.Equals("test"))
            {
                if (testId != null)
                {
                    attempts = attempts.Where(attempt => attempt.TestId == testId).ToList();
                }

                var attemptIds = new List<int>();
                foreach(Attempt attempt in attempts)
                {
                    attemptIds.Add(attempt.Id);
                }

                responses = responses.Where(response => attemptIds.Contains(response.Attempt.Id)).ToList();

                model.Tests = prepareTestIndex(attempts, searchTerm, sortBy, sortMode, page);
            }

            model.TestsTaken = attempts.Count;
            model.UniqueTests = attempts.GroupBy(attempt => attempt.Test.Id).Select(g => g.First()).ToList().Count;

            var correctResponses = 0;
            foreach (Response response in responses)
            {
                if (response.Option.IsCorrect)
                {
                    correctResponses++;
                }
            }
            model.AverageScore = (double)correctResponses / responses.Count;

            ViewBag.SelectedMode = mode;

            ViewBag.Mode = new SelectList(model.Modes, "Value", "Text", mode);

            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Description", subject);           

            return View(model);
        }

        /// <summary>
        /// Method to calculate age from DoB
        /// </summary>
        /// <param name="dob">Date of Birth</param>
        /// <returns>The current age of the person based on DoB</returns>
        private int calculateAge(DateTime dob)
        {
            var age = DateTime.Today.Year - dob.Year;
            if (DateTime.Today < dob.AddYears(age))
            {
                age--;
            }
            return age;
        }

        /// <summary>
        /// Prepares an IPagedList of test attempts to populate the table in the User Statistics page
        /// </summary>
        /// <param name="attempts">An unorganized list of the test attempts made</param>
        /// <param name="searchTerm">Search string to filter by</param>
        /// <param name="sortBy">Column to sort on</param>
        /// <param name="sortMode">Ascending or descending</param>
        /// <param name="page">The current page number</param>
        /// <returns></returns>
        private IPagedList<UserTestStatisticsViewModel> prepareTestIndex(List<Attempt> attempts, string searchTerm, string sortBy, string sortMode, int page)
        {
            //Convert to TestStatisticsViewModel
            var testStatisticsViewModels = new List<UserTestStatisticsViewModel>();
            for (int i = 0; i < attempts.Count; i++)
            {

                var testStatisticsViewModel = new UserTestStatisticsViewModel();
                testStatisticsViewModel.Test = attempts[i].Test;
                testStatisticsViewModel.DateTaken = attempts[i].EndTime;
                testStatisticsViewModel.CorrectResponses = 0;
                testStatisticsViewModel.Subjects = new List<string>();

                foreach (Response response in attempts[i].Responses)
                {
                    if (response.Option.IsCorrect)
                    {
                        testStatisticsViewModel.CorrectResponses++;
                    }
                }
                testStatisticsViewModel.Score = (double)testStatisticsViewModel.CorrectResponses / testStatisticsViewModel.Test.TestQuestions.Count;

                var questionIds = attempts[i].Test.TestQuestions.Select(tq => tq.QuestionId).ToList();
                var questions = db.Questions.Where(q => questionIds.Contains(q.Id)).ToList();
                for (int j = 0; j < questions.Count; j++)
                {
                    if (!testStatisticsViewModel.Subjects.Contains(questions[j].Subject.Description))
                    {
                        testStatisticsViewModel.Subjects.Add(questions[j].Subject.Description);
                    }
                }

                //Reverse sort Subjects if page is sorted by subject in descending order, else sort in alpha order
                testStatisticsViewModel.Subjects = sortBy != null && sortBy.Equals("subject") && sortMode != null && sortMode.Equals("desc") ?
                                              testStatisticsViewModel.Subjects.OrderByDescending(s => s).ToList() :
                                              testStatisticsViewModel.Subjects.OrderBy(s => s).ToList();

                testStatisticsViewModels.Add(testStatisticsViewModel);
            }

            //Search
            if (searchTerm != null)
            {
                testStatisticsViewModels = testStatisticsViewModels.Where(t => t.Test.TestName.ToLower().Contains(searchTerm.ToLower()) ||
                                                                          t.Test.AspNetUser.UserName.ToLower().Contains(searchTerm.ToLower()) ||
                                                                          t.Test.TestQuestions.Count.ToString().ToLower().Contains(searchTerm.ToLower()) ||
                                                                          t.Subjects.Where(s => s.ToLower().Contains(searchTerm.ToLower())).ToList().Count > 0).ToList();
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("subject"))
                {
                    testStatisticsViewModels = sortMode != null && sortMode.Equals("desc") ?
                                               testStatisticsViewModels.OrderByDescending(t => t.Subjects.First()).ToList() :
                                               testStatisticsViewModels.OrderBy(t => t.Subjects.First()).ToList();
                }
                else if (sortBy.Equals("creator"))
                {
                    testStatisticsViewModels = sortMode != null && sortMode.Equals("desc") ?
                                               testStatisticsViewModels.OrderByDescending(t => t.Test.AspNetUser.UserName).ToList() :
                                               testStatisticsViewModels.OrderBy(t => t.Test.AspNetUser.UserName).ToList();
                }
                else if (sortBy.Equals("score"))
                {
                    testStatisticsViewModels = sortMode != null && sortMode.Equals("desc") ?
                                               testStatisticsViewModels.OrderByDescending(t => t.Score).ToList() :
                                               testStatisticsViewModels.OrderBy(t => t.Score).ToList();
                }
                else if (sortBy.Equals("datetaken"))
                {
                    testStatisticsViewModels = sortMode != null && sortMode.Equals("desc") ?
                                               testStatisticsViewModels.OrderByDescending(t => t.DateTaken).ToList() :
                                               testStatisticsViewModels.OrderBy(t => t.DateTaken).ToList();
                }
                else if (sortBy.Equals("updated"))
                {
                    testStatisticsViewModels = sortMode != null && sortMode.Equals("desc") ?
                                               testStatisticsViewModels.OrderByDescending(t => t.Test.UpdatedDate).ToList() :
                                               testStatisticsViewModels.OrderBy(t => t.Test.UpdatedDate).ToList();
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                testStatisticsViewModels = testStatisticsViewModels.OrderByDescending(t => t.Test.TestName).ToList();
            }
            else
            {
                testStatisticsViewModels = testStatisticsViewModels.OrderBy(t => t.Test.TestName).ToList();
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return testStatisticsViewModels.ToPagedList(page, PageSize);
        }
    }
}
