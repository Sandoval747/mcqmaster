﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using mcqMaster.Models;
using mcqMaster.DAL;
using System.Collections.Generic;
using PagedList;

namespace mcqMaster.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly int PageSize = 20;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Display the view for the login page
        /// </summary>
        /// <param name="returnUrl">URL of the sender stored for redirect on successsful login</param>
        /// <returns>Login View</returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        /// <summary>
        /// Enacts login on POST from login form
        /// </summary>
        /// <param name="model">The ViewModel of the Login form</param>
        /// <param name="returnUrl">URL of the sender stored for redirect on successsful login</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Adds an error message if user is banned and redirects to login screen
            var user = UserManager.FindByName(model.UserName);
            if (user.IsBanned)
            {
                ModelState.AddModelError("UserName", "User is banned");
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Description");
            return View();
        }

        /// <summary>
        /// Registers the user upon submission of the Register form
        /// </summary>
        /// <param name="model">The ViewModel of the Register Form</param>
        /// <returns>The view to display depending on the result.Successful registration logs the user in and returns to Home</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateOfBirth = model.DateOfBirth,
                    GenderId = model.GenderId,
                    Gender = model.Gender,
                    SecurityQuestion = model.SecurityQuestion,
                    SecurityAnswer = UserManager.PasswordHasher.HashPassword(model.SecurityAnswer)
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await this.UserManager.AddToRoleAsync(user.Id, "User");
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Description", model.GenderId);
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.UsernameOrEmail);
                if (user == null)
                {
                    user = await UserManager.FindByEmailAsync(model.UsernameOrEmail);
                }
                if (user != null)
                {
                    return View("SecurityQuestion", new SecurityQuestionViewModel { UserName = user.UserName, SecurityQuestion = user.SecurityQuestion });
                }
                else
                {
                    ModelState.AddModelError("UsernameOrEmail", "No account found with provided information");
                }
                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// Evaluates if Security Question is correct
        /// </summary>
        /// <param name="model">ViewModel for Security Question</param>
        /// <returns>The view to display in response. Redirects to Reset Password on success</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SecurityQuestion(SecurityQuestionViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.UserName);
                if (user.SecurityAnswer.Equals(model.SecurityAnswer))
                {
                    return View("ResetPassword", new ResetPasswordViewModel { UserName = model.UserName });
                }
            }
            return View(model);
        }

        /// <summary>
        /// The reset password page
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            else
            {
                user = db.Users.Find(user.Id);
                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                db.SaveChanges();
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            //AddErrors(result);
            //return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        /// <summary>
        /// Displays a list of users to the Administrator with options to search, sort and paginate
        /// </summary>
        /// <param name="searchTerm">The search term to use on the list of users</param>
        /// <param name="sortBy">What field to sort by on the list of users</param>
        /// <param name="sortMode">Whether to sort descending or ascending</param>
        /// <param name="page">The page number of the results</param>
        /// <returns>Returns a view of the user index, modified as per the params</returns>
        [Authorize(Roles = "Admin")]
        //GET: /Account/UserIndex
        public ActionResult UserIndex(string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            var users = new List<ApplicationUser>();
            users = db.Users.ToList();

            var model = prepareUserIndex(users, searchTerm, sortBy, sortMode, page);

            return View(model);
        }

        /// <summary>
        /// Prepares an IPagedList of users for the UserIndex
        /// </summary>
        /// <param name="users">An unorganized list of users</param>
        /// <param name="searchTerm">The search term to apply to the list as a filter</param>
        /// <param name="sortBy">The field on which to sort the users</param>
        /// <param name="sortMode">Whether to sort scending or descending</param>
        /// <param name="page">The page number of the results</param>
        /// <returns></returns>
        public IPagedList<ApplicationUser> prepareUserIndex(List<ApplicationUser> users, string searchTerm, string sortBy, string sortMode, int page = 1)
        {
            //Search
            if (searchTerm != null)
            {
                users = users.Where(u => u.UserName.Contains(searchTerm) ||
                                    u.LastName.Contains(searchTerm) ||
                                    u.FirstName.Contains(searchTerm)).ToList();
            }

            //Sort
            if (sortBy != null)
            {
                if (sortBy.Equals("name"))
                {
                    users = sortMode != null && sortMode.Equals("desc") ?
                            users.OrderByDescending(u => u.LastName).ToList() :
                            users.OrderBy(u => u.LastName).ToList();
                }
                else if (sortBy.Equals("email"))
                {
                    users = sortMode != null && sortMode.Equals("desc") ?
                            users.OrderByDescending(u => u.Email).ToList() :
                            users.OrderBy(u => u.Email).ToList();
                }
                else if (sortBy.Equals("isbanned"))
                {
                    users = sortMode != null && sortMode.Equals("desc") ?
                            users.OrderByDescending(u => u.IsBanned).ToList() :
                            users.OrderBy(u => u.IsBanned).ToList();
                }
            }
            else if (sortMode != null && sortMode.Equals("desc"))
            {
                users = users.OrderByDescending(u => u.UserName).ToList();
            }
            else
            {
                users = users.OrderBy(u => u.UserName).ToList();
            }

            //Prepare ViewBag
            ViewBag.SearchTerm = searchTerm;
            ViewBag.SortBy = sortBy;
            ViewBag.SortMode = sortMode;
            ViewBag.Page = page;

            return users.ToPagedList(page, PageSize);
        }

        /// <summary>
        /// Grants admin privileges to the specified user
        /// </summary>
        /// <param name="id">The user ID of the new Admin</param>
        /// <returns>JSON response that the method was successful or failed</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmpowerAdmin(string id)
        {
            var user = db.Users.Find(id);
            UserManager.AddToRole(user.Id, "Admin");
            db.SaveChanges();
            return Json(user.UserName + "was made an admin", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bans the specified user
        /// </summary>
        /// <param name="id">The user ID of the user to ban</param>
        /// <returns>JSON response indicating success or failure</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BanUser(string id)
        {
            var user = db.Users.Find(id);
            user.IsBanned = true;
            db.SaveChanges();
            return Json(user.UserName + "was banned", JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}